package com.qf.watchshop.myutil;

public interface MessageCodeConstants {
    String REGISTER_FAIL = "700";
    String LOGIN_FAIL = "800";
    String LOGIN_TIME_OUT = "900";
    String PRODUCT_LIST_NO_DATA = "1000";
}
