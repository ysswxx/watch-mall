package com.qf.watchshop.myzuul.util;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthFilter extends ZuulFilter {
    @Value("${white.path}")
    private String whitePath;


    @Override
    public String filterType() {
        return FilterConstants.PRE_TYPE;
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {
               HttpServletResponse response = RequestContext.getCurrentContext().getResponse();
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        System.out.println("过滤器正在执行...");
        String token = request.getHeader("token");
        response.setContentType("application/json;charset=utf-8");
        ObjectMapper mapper = new ObjectMapper();


       /* if(!isWhitePath(request.getRequestURI())) {
            //拦截不走路由
            requestContext.setSendZuulResponse(false);

        }*/
       /* final String s;
        try {
            s = mapper.writeValueAsString(data);
            System.out.println(s);
            response.getWriter().write(s);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/
        /*isWhitePath(whitePath);*/


        return null;
    }

    private boolean isWhitePath(String uri){
        if (whitePath != null){
            final String[] strings = whitePath.split(",");
            if (strings != null){
                for (String string : strings) {
                    if (uri.contains(string)){
                        return true;
                    }
                }
            }
        }
        return false;
    }
}
