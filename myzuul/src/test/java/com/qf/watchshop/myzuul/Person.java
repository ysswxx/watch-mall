package com.qf.watchshop.myzuul;

/**
 * 作者: 官官
 * 时间：2021/4/3 13:43
 */
public interface Person {

    void read();
    void write();
}
