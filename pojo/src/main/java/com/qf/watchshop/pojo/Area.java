package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 凯诺
 * 时间：2021/3/24 19:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("area")
public class Area {
    @TableId(value = "area_id",type = IdType.NONE)
    private Integer areaId;
    @TableField("area_name")
    private String areaName;
    @TableField("city_id")
    private Integer cityId;
}
