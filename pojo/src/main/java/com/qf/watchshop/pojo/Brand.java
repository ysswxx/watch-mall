package com.qf.watchshop.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Brand {

  private Integer brandId;
  private String brandName;
  private String field1;
  private String field2;
  private String field3;




}
