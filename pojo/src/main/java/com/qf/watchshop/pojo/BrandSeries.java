package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 凯诺
 * 时间：2021/3/24 19:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("brand_series")
public class BrandSeries {
    @TableId(value = "brandId",type = IdType.AUTO)
    private Integer brandId;//品牌编号
    private String name;//品牌名或者系列名
    private Integer parentId;//父节点（品牌编号）
    private String field1;
    private String field2;
    private String field3;
}
