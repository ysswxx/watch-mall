package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 凯诺
 * 时间：2021/3/24 19:12
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("city")
public class City {
    @TableId(value = "city_id",type = IdType.NONE)
    private Integer cityId;
    @TableField("city_name")
    private String cityName;
    @TableField("province_id")
    private String provinceId;
    @TableField("first_letter")
    private String firstLetter;
    @TableField("is_hot")
    private Integer isHot;
    private Integer state;
}
