package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("collect")
public class Collect {
  @TableId(value = "collectId",type = IdType.AUTO)
  private Integer collectId;//收藏编号
  @TableField("userId")
  private Integer userId;//用户编号
  @TableField("watchId")
  private Integer watchId;//商品编号
}
