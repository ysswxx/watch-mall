package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("customer")
public class Customer {
  @TableId(value = "customerId",type = IdType.AUTO)
  private Integer customerId;
  private String customerName;
  private String customerType;
  private String address;
  private String customerInfo;
  private String field1;
  private String field2;
  private String field3;
}
