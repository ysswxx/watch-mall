package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("module")
public class Module {

  @TableField("module_code")
  private String moduleCode;
  @TableField("module_name")
  private String moduleName;
  @TableField("link_url")
  private String linkUrl;
  @TableField("module_order")
  private Integer moduleOrder;
  @TableField("parent_module")
  private String parentModule;
  @TableField("module_desc")
  private String moduleDesc;
  @TableField("expanded")
  private String expanded;
  @TableField("leaf")
  private String leaf;




}
