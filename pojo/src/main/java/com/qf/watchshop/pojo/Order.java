package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("orders")
public class Order implements Serializable {
  @TableId(value = "orderId")
  private String orderId;
  @TableField("watchId")
  private Integer watchId;
  @TableField("state")
  private String state;
  @TableField("price")
  private Double price;
  @TableField("region")
  private String region;
  @TableField("address")
  private String address;
  @TableField("zipcode")
  private String zipcode;
  @TableField("receiver")
  private String receiver;
  @TableField("tel")
  private String tel;
  @TableField("mail")
  private String mail;




}
