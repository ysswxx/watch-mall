package com.qf.watchshop.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.lang.ref.PhantomReference;

/**
 * 作者: 官官
 * 时间：2021/3/30 12:21
 */
@NoArgsConstructor
@Data
@AllArgsConstructor
public class ProductDetails {

    private Watch watch;
    private Watchtype watchtype;

    private Brand brand;
    private Series series;
}
