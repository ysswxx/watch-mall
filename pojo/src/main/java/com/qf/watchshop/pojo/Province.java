package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 凯诺
 * 时间：2021/3/24 19:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "province")
public class Province {
    @TableId(value = "province_id",type = IdType.NONE)
    private String provinceId;
    @TableField("province_name")
    private String provinceName;
}
