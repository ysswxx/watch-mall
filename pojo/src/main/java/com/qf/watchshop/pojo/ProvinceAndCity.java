package com.qf.watchshop.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 官官
 * 时间：2021/4/2 10:02
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProvinceAndCity {

    private String provinceName;
    private String cityName;
}
