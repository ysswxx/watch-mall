package com.qf.watchshop.pojo;

import lombok.Data;

@Data
public class ResultData implements java.io.Serializable{
    private String code; // 查询结果编码，成功为10000，其他为失败
    private String desc; // 失败描述
    private Object data; // 成功数据

    private ResultData(){}

    public static com.qf.watchshop.pojo.ResultData createSuccessJsonResult(Object data){
        com.qf.watchshop.pojo.ResultData vo = new com.qf.watchshop.pojo.ResultData();
        vo.code = "10000";
        vo.data = data;
        return vo;
    }

    public static com.qf.watchshop.pojo.ResultData createFailJsonResult(String code, String desc){
        com.qf.watchshop.pojo.ResultData vo = new com.qf.watchshop.pojo.ResultData();
        vo.code = code;
        vo.desc = desc;
        return vo;
    }
}
