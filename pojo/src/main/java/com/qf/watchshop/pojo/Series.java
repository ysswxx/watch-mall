package com.qf.watchshop.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Series {

  private Integer seriesId;
  private Integer brandId;
  private String seriesName;
  private String field1;
  private String field2;




}
