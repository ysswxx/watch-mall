package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jdk.nashorn.internal.ir.annotations.Ignore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("users")
public class Users {
  @TableId(value = "userId", type = IdType.AUTO)
  private Integer userId;
  @TableField("customerId")
  private String customerId;
  @TableField("username")
  private String username;
  @TableField("password")
  private String password;
  @TableField("email")
  private String email;
  @TableField("tel")
  private String tel;
  @TableField("account")
  private Double account;
  @TableField("qq")
  private String qq;
  @TableField("wechart")
  private String wechart;
  @TableField("salt")
  private String salt;

  @TableField("true_name")
  private String turename;
  private String field2;
  private String field3;

  //获取验证码
  @TableField(exist = false)
  private String code;




}
