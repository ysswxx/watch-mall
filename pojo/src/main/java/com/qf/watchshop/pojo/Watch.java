package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("watch")
public class Watch {

  @TableId(value = "watchId", type = IdType.AUTO)
  private Integer watchId;
  @TableField("userId")
  private Integer userId;
  //商户id
  @TableField("customerId")
  private Integer customerId;
  @TableField("watchName")
  private String watchName;
  @TableField("price")
  private Double price;
  //商品的成色(未使用、非常好、良好....)
  @TableField("state")
  private Integer state;
  @TableField("image1")
  private String image1;
  @TableField("image2")
  private String image2;
  @TableField("image3")
  private String image3;
  @TableField("movementType")
  private String movementType;
  @TableField("size")
  private Double size;
  @TableField("waterProof")
  private Integer waterProof;
  @TableField("watchStrapType")
  private String watchStrapType;
  @TableField("watchStrapColor")
  private String watchStrapColor;
  @TableField("watchButtonType")
  private String watchButtonType;
  @TableField("dialPlateColor")
  private String dialPlateColor;
  @TableField("dialPlateType")
  private String dialPlateType;
  @TableField("createtime")
  private Date createTime;
  @TableField("typeId")
  private String typeId;
  @TableField("seriesId")
  private String seriesId;
  private String field3;
  private String field4;
  private String field5;




}
