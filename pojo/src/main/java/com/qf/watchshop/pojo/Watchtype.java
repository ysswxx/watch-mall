package com.qf.watchshop.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("watchtype")
public class Watchtype {

  private Integer typeId;
  private String typeName;
  private String watchId;
  private String field1;
  private String field2;
  private String field3;




}
