package com.qf.productserver.controller;


import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.qf.productserver.util.WebSocket;
import com.qf.watchshop.myutil.PayCommonUtil;
import com.qf.watchshop.myutil.PayConfigUtil;
import com.qf.watchshop.myutil.XMLUtil;
import com.qf.watchshop.myutil.ZxingUtil;
import com.qf.watchshop.pojo.ResultData;
import org.aspectj.weaver.MemberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

/**
 * 作者: 官官
 * 时间：2021/4/2 14:19
 */
@Controller
@SessionAttributes({"image"})
public class PayController {
    @PostMapping("/buy")
    @ResponseBody

    public ResultData buy(String proName, Model model) throws Exception {
        // 生成订单
        String price = "1";//此处默认是1分,次数需要项目开发中实际根据用户购买的商品获取
        Long id = IdWorker.getId();
        String url = PayCommonUtil.weixin_pay(price, proName, id.toString());//获取微信返回的二维码对应的短地址
        // 将url转换成base64格式，以方便后面传参
        String string = Base64.getEncoder().encodeToString(url.getBytes());
        System.out.println(string);
        Map map = new HashMap();

        map.put("orderId", id.toString());
        map.put("url",string);
        return ResultData.createSuccessJsonResult(map);
    }
    @RequestMapping("/showImage")
    public void showImage(String urlStr, HttpServletResponse response)throws Exception{
        System.out.println(urlStr);
        // 将传递的base64格式的url反编码
        String url = new String(Base64.getDecoder().decode(urlStr));
        System.out.println(url);
        BufferedImage image = ZxingUtil.createImage(url, 300, 300);//将地址转成二维码图片
        if (image != null){
            ImageIO.write(image, "JPEG", response.getOutputStream());
        }
    }

    @RequestMapping("/success")
    public void success(HttpServletRequest request, HttpServletResponse response)throws Exception{
        System.out.println("支付成功");
        weixin_notify(request, response);
    }

//    @RequestMapping("/testwb")
//    public void testwb(){
//        String jsonString = "{\"code\":\"10000\", \"desc\":\"支付成功,5秒后跳转页面\", \"url\":\"success.html\"}";
//        //实际开发中,应该返回json 数据,这样页面可以解析,可以根据结果来决定做什么,此处只返回支付成功
//        WebSocket.sendMessage("111",jsonString,null);
//    }

//    @RequestMapping("/successPage")
//    public String successPage(){
//        return "/success";
//    }

    /**
     * 解析微信返回的支付结果
     * @param request
     * @param response
     * @throws Exception
     */
    public void weixin_notify(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String writeContent="默认支付失败";
//        String path = request.getServletContext().getRealPath("file");
//        File file = new File(path);
//        if (!file.exists()) {
//            file.mkdirs();
//        }
//        FileOutputStream fileOutputStream = new FileOutputStream(path+"/result.txt", true);
        //读取参数
        InputStream inputStream ;
        StringBuffer sb = new StringBuffer();
        inputStream = request.getInputStream();
        String s ;
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
        while ((s = in.readLine()) != null){
            sb.append(s);
        }
        in.close();
        inputStream.close();

        //解析xml成map
        Map<String, String> m = new HashMap<String, String>();
        m = XMLUtil.doXMLParse(sb.toString());

        //过滤空 设置 TreeMap
        SortedMap<Object,Object> packageParams = new TreeMap<Object,Object>();
        Iterator it = m.keySet().iterator();
        while (it.hasNext()) {
            String parameter = (String) it.next();
            String parameterValue = m.get(parameter);

            String v = "";
            if(null != parameterValue) {
                v = parameterValue.trim();
            }
            packageParams.put(parameter, v);
        }

        // 账号信息
        String key = PayConfigUtil.API_KEY; // key

        System.err.println(packageParams);
        String out_trade_no = (String)packageParams.get("out_trade_no");
        System.out.println(out_trade_no);
        //判断签名是否正确
        if(PayCommonUtil.isTenpaySign("UTF-8", packageParams,key)) {
            //------------------------------
            //处理业务开始
            //------------------------------
            String resXml = "";
            if("SUCCESS".equals((String)packageParams.get("result_code"))){
                // 这里是支付成功
                //////////执行自己的业务逻辑////////////////
                String mch_id = (String)packageParams.get("mch_id");
                String openid = (String)packageParams.get("openid");
                String is_subscribe = (String)packageParams.get("is_subscribe");
                // String out_trade_no = (String)packageParams.get("out_trade_no");

                String total_fee = (String)packageParams.get("total_fee");

                System.err.println("mch_id:"+mch_id);
                System.err.println("openid:"+openid);
                System.err.println("is_subscribe:"+is_subscribe);
                System.err.println("out_trade_no:"+out_trade_no);
                System.err.println("total_fee:"+total_fee);

                // TODO 执行自己的业务逻辑
                //////////执行自己的业务逻辑////////////////

                System.err.println("支付成功");
                writeContent = "订单:" + out_trade_no + "支付成功";
                //通知微信.异步确认成功.必写.不然会一直通知后台.八次之后就认为交易失败了.
                resXml = "<xml>" + "<return_code><![CDATA[SUCCESS]]></return_code>"
                        + "<return_msg><![CDATA[OK]]></return_msg>" + "</xml> ";


                String jsonString = "{\"code\":\"10000\", \"desc\":\"支付成功,5秒后跳转页面\", \"url\":\"success.html\"}";

                //实际开发中,应该返回json 数据,这样页面可以解析,可以根据结果来决定做什么,此处只返回支付成功
                WebSocket.sendMessage(out_trade_no,jsonString,null);

            } else {
                writeContent = "订单"+out_trade_no+"支付失败,错误信息：" + packageParams.get("err_code");
                System.err.println("订单"+out_trade_no+"支付失败,错误信息：" + packageParams.get("err_code"));
                resXml = "<xml>" + "<return_code><![CDATA[FAIL]]></return_code>"
                        + "<return_msg><![CDATA[报文为空]]></return_msg>" + "</xml> ";
            }
            //------------------------------
            //处理业务完毕
            //------------------------------
            BufferedOutputStream out = new BufferedOutputStream(
                    response.getOutputStream());
            out.write(resXml.getBytes());
            out.flush();
            out.close();
        } else{
            writeContent = "订单"+out_trade_no+"通知签名验证失败,支付失败";
            System.err.println("通知签名验证失败");
        }
//        fileOutputStream.write(writeContent.getBytes());
//        fileOutputStream.close();
    }
}
