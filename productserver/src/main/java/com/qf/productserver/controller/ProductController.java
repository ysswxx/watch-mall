package com.qf.productserver.controller;

import com.qf.productserver.mapper.ProductMapper;
import com.qf.productserver.service.ProductService;
import com.qf.productserver.util.MySender;
import com.qf.watchshop.pojo.Order;
import com.qf.watchshop.pojo.ProductDetails;
import com.qf.watchshop.pojo.ResultData;
import com.qf.watchshop.pojo.Watch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 作者: 官官
 * 时间：2021/3/28 12:08
 */
@RestController
public class ProductController {
    @Resource
    ProductService ps;

    @Autowired
    MySender ms;



    @GetMapping("/selectone/{id}")

    public ResultData selectone(@PathVariable("id") Integer id){
        return ps.selectById(id);


    }
    //收藏商品
    @GetMapping("/collect")

    public void collect(){
        Integer useid=1;

        Integer watchid=202120;
        System.out.println("收藏后台进入");
        ps.collect(useid,watchid);



    }

    @PostMapping("/selectdetails")

    public ResultData selectdetails(@RequestBody Watch watch){

        System.out.println("收藏后台进入");


        ProductDetails details= ps.selectprodctdetails(watch.getUserId(),watch.getWatchId());



        return ResultData.createFailJsonResult("200","创建成功");

    }

    //查看排行榜
    @GetMapping("/selectsort")
    public ResultData selectsort(){








        return ps.selectsort();

    }


    @PostMapping("/saveaddress")

    public ResultData saveaddress(@RequestBody Order order){


        return ps.saveaddress(order);
    }


    @GetMapping("/province")
    public ResultData province(){








        return ps.selectprovince();

    }

    @PostMapping("/orderinsert")

    public ResultData orderinsert(@RequestBody Order order){
        order.setState("0");



        return ps.orderinsert(order);

    }
    //支付成功改变状态
    @GetMapping("/updatestate")

    public ResultData updatestate(String orderId){



        return ps.updatestate(orderId);

    }

    //超时改变状态
    @PostMapping("/order")

    public void mqadd(@RequestBody Order order){
        //调用生产者发送消息
        ms.sendOrder(order);




    }


}
