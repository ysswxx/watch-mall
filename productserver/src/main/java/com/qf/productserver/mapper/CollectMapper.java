package com.qf.productserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.watchshop.pojo.Collect;

/**
 * 作者: 官官
 * 时间：2021/3/29 15:11
 */
public interface CollectMapper extends BaseMapper<Collect> {
}
