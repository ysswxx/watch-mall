package com.qf.productserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.watchshop.pojo.Order;

/**
 * 作者: 官官
 * 时间：2021/4/2 9:05
 */
public interface OrderMapper extends BaseMapper<Order> {
}
