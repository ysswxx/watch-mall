package com.qf.productserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.watchshop.pojo.Watch;

/**
 * 作者: 官官
 * 时间：2021/3/28 12:13
 */
public interface ProductMapper extends BaseMapper<Watch> {
}
