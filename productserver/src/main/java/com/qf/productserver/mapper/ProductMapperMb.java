package com.qf.productserver.mapper;

import com.qf.productserver.util.WatchSort;
import com.qf.watchshop.pojo.ProductDetails;
import com.qf.watchshop.pojo.ProvinceAndCity;
import com.qf.watchshop.pojo.Watch;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 作者: 官官
 * 时间：2021/3/30 12:04
 */
public interface ProductMapperMb {

    ProductDetails selectprodctdetails(@Param("useId") Integer useid, @Param("watchId")Integer watchid);

    List<WatchSort> selectsort();

    List<ProvinceAndCity> selectProvinceAndCity();
}
