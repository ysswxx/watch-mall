package com.qf.productserver.service;

import com.qf.watchshop.pojo.Order;
import com.qf.watchshop.pojo.ProductDetails;
import com.qf.watchshop.pojo.ResultData;

/**
 * 作者: 官官
 * 时间：2021/3/28 12:20
 */
public interface ProductService {
    ResultData selectById(Integer id);

    void collect(Integer useid,Integer watchid);

    ProductDetails selectprodctdetails(Integer useid, Integer watchid);

    ResultData selectsort();


    void updatesort();

    ResultData saveaddress(Order order);

    ResultData selectprovince();

    ResultData orderinsert(Order order);

    ResultData updatestate(String orderId);
}
