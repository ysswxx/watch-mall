package com.qf.productserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.qf.productserver.mapper.CollectMapper;
import com.qf.productserver.mapper.OrderMapper;
import com.qf.productserver.mapper.ProductMapper;
import com.qf.productserver.mapper.ProductMapperMb;
import com.qf.productserver.service.ProductService;
import com.qf.productserver.util.FileToHtml;
import com.qf.productserver.util.WatchSort;
import com.qf.watchshop.pojo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundValueOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * 作者: 官官
 * 时间：2021/3/28 12:20
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    RedisTemplate redisTemplate;

    @Resource
    OrderMapper om;
    @Resource
    ProductMapperMb pmn;
    @Resource
    ProductMapper pm;
    @Resource
    CollectMapper cm;

    @Override
    public ResultData selectById(Integer id) {
        Watch watch = pm.selectById(id);
        try {
            //网页静态化
            new FileToHtml().ftlToHtml("watchdetail"+watch.getWatchId(),watch);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResultData.createSuccessJsonResult(watch);
    }

    @Override
    public void collect(Integer useid, Integer watchid) {
        Collect collect = new Collect();
        collect.setUserId(useid);
        collect.setWatchId(watchid);
        cm.insert(collect);
    }

    @Override
    public ProductDetails selectprodctdetails(Integer useid, Integer watchid) {


        BoundValueOperations op = redisTemplate.boundValueOps("watchdetail" + watchid);

        if (op.get()!=null){
            return null;

        }
        ProductDetails selectprodctdetails = pmn.selectprodctdetails(useid, watchid);


        try {
            //网页静态化
            new FileToHtml().ftlToHtml("watchdetail"+selectprodctdetails.getWatch().getWatchId(),selectprodctdetails);
            op.set("添加模板");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return selectprodctdetails;
    }

    @Override
    public ResultData selectsort() {





        Set set = redisTemplate.opsForZSet().reverseRange("watchsort", 0, -1);


        return ResultData.createSuccessJsonResult(set);
    }

    @Override
    @Scheduled(cron = "0 11 21 */1 * ?")
    public void updatesort() {
        System.out.println("定时任务执行");
        List<WatchSort> selectsort = pmn.selectsort();
        System.out.println(selectsort);
        for (WatchSort watchSort : selectsort) {
            redisTemplate.opsForZSet().add("watchsort",watchSort.getWatchName(),watchSort.getScore());
        }
    }

    @Override
    public ResultData saveaddress(Order order) {
        int i = om.insert(order);
        if (i>0){
            return ResultData.createFailJsonResult("200","插入成功");
        }

        return ResultData.createFailJsonResult("500","插入失败");
    }

    @Override
    public ResultData selectprovince() {
        List<ProvinceAndCity> andCities = pmn.selectProvinceAndCity();
        return ResultData.createSuccessJsonResult(andCities);
    }

    @Override
    public ResultData orderinsert(Order order) {
        Long id = IdWorker.getId();
        order.setOrderId(id.toString());
        int i = om.insert(order);

        if (i>0){
            return ResultData.createSuccessJsonResult(order.getOrderId());
        }
        return ResultData.createFailJsonResult("500","生成失败");
    }

    @Override
    public ResultData updatestate(String orderId) {


        Order order = new Order();
        order.setOrderId(orderId);
        order.setState("2");
        int i = om.updateById(order);
        if (i>0){
            return ResultData.createSuccessJsonResult(i);
        }

        return ResultData.createFailJsonResult("500","更新失败");
    }




}
