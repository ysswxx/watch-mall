package com.qf.productserver.util;

import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.qf.watchshop.pojo.Watch;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 作者: 官官
 * 时间：2021/3/28 12:27
 */
public class FileToHtml {
    private static final Logger LOGGER = LoggerFactory.getLogger(FileToHtml.class) ;
    private static final String PATH = "/templates/" ;


    public String ftlToHtml(String name,Object productdetail) throws Exception {
        // 创建配置类
        Configuration configuration = new Configuration(Configuration.getVersion());
        // 设置模板路径
        String classpath = this.getClass().getResource("/").getPath();
        configuration.setDirectoryForTemplateLoading(new File(classpath + PATH));
        // 加载模板
        Template template = configuration.getTemplate("productdetail.ftl");
        // 数据模型

        Map<String, Object> map = new HashMap<>();
        System.out.println(productdetail);

        map.put("productdetail", productdetail);
        map.put("myTitle","官官的标题");


        // 静态化页面内容
        String content = FreeMarkerTemplateUtils.processTemplateIntoString(template, map);
        LOGGER.info("content:{}",content);
        InputStream inputStream = IOUtils.toInputStream(content,"UTF-8");
        String fimename="D:\\Hbiuderqf\\mailegebiao\\"+name+".html";
        // 输出文件
        FileOutputStream fileOutputStream = new FileOutputStream(new File(fimename));
        IOUtils.copy(inputStream, fileOutputStream);
        // 关闭流
        inputStream.close();
        fileOutputStream.close();

        return fimename;
    }


}
