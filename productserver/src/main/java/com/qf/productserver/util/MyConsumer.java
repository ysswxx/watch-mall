package com.qf.productserver.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.productserver.mapper.OrderMapper;

import com.qf.watchshop.pojo.Order;
import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

/**
 * 作者: 官官
 * 时间：2021/3/11 19:11
 */
@Component
public class MyConsumer {
    @Resource
    private OrderMapper orderDAO;

   /* @RabbitListener(queues = "simpleQueue")
    public void consume(Order orders,  Channel channel, Message msg) throws IOException {
        orderDAO.insert(orders);
        channel.basicAck(msg.getMessageProperties().getDeliveryTag(), false);
        System.out.println(orders.getId() + "添加成功");

    }*/
   @RabbitListener(queues = "deadQueue")
   public void consume(Order orders, Channel channel, Message msg) throws IOException {
       System.out.println("消费者在消费");
       orders.setState("2");
       QueryWrapper queryWrapper = new QueryWrapper<>();
      orderDAO.updateById(orders);
       channel.basicAck(msg.getMessageProperties().getDeliveryTag(), false);
       System.out.println(orders.getOrderId() + "取消成功");
   }
}