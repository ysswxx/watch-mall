package com.qf.productserver.util;


import com.qf.watchshop.pojo.Order;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 作者: 官官
 * 时间：2021/3/11 19:13
 */
@Component
public class MySender {
    @Resource
    private RabbitTemplate rabbitTemplate;

    /*public void sendOrder(Order order) {
        order.setName("华为");
        order.setState("0");

        rabbitTemplate.convertAndSend("simpleExchange", "simpleKey", order);
    }*/

    //死信队列
    public void sendOrder(Order orders){
        rabbitTemplate.convertAndSend("orderDelayExchange", "orderDelayKey", orders, message -> {
                    // 设置超时时间,单位毫秒
                    message.getMessageProperties().setExpiration("10000");
                    return message;
                }
        );
        System.out.println(orders.getOrderId() + "订单已经放入队列中...");
    }


}