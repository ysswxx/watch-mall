package com.qf.productserver.util;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 作者: 官官
 * 时间：2021/3/11 19:05
 */
@Configuration
public class RabbitConfig {
   /* // 创建队列
    @Bean
    public Queue simpleQueue(){
        return new Queue("simpleQueue");
    }

    // 创建Direct类型交换机
    @Bean
    public DirectExchange simpleExchange(){
        return new DirectExchange("simpleExchange");
    }

    // 将队列绑定到交换机
    @Bean
    public Binding simpleBinding(){
        return BindingBuilder.bind(simpleQueue()).to(simpleExchange()).with("simpleKey");
    }*/
   // 创建延迟队列绑定死信交换机
   @Bean
   public Queue orderDelayQueue(){
       // 将订单延迟队列和死信队列建立关联
       Map map = new HashMap();
       map.put("x-dead-letter-exchange", "dead_exchange");
       map.put("x-dead-letter-routing-key", "dead_route_key");
       return new Queue("orderDelayQueue", true, false, false, map);
   }

    // 创建Direct类型交换机
    @Bean
    public DirectExchange orderDelayExchange(){
        return new DirectExchange("orderDelayExchange");
    }

    // 将延迟队列绑定到延迟交换机
    @Bean
    public Binding simpleBinding(){
        return BindingBuilder.bind(orderDelayQueue()).to(orderDelayExchange()).with("orderDelayKey");
    }

    // 创建死信队列
    @Bean
    public Queue deadQueue(){
        // 将订单延迟队列绑定死信队列
        return new Queue("deadQueue");
    }

    // 创建死信Direct类型交换机
    @Bean
    public DirectExchange deadExchange(){
        return new DirectExchange("dead_exchange");
    }

    // 将队列绑定到交换机
    @Bean
    public Binding deadBinding(){
        return BindingBuilder.bind(deadQueue()).to(deadExchange()).with("dead_route_key");
    }
}
