package com.qf.productserver.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者: 官官
 * 时间：2021/4/1 20:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WatchSort {

    private String watchName;
    private Integer score;
}
