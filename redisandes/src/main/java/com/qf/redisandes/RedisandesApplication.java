package com.qf.redisandes;

import com.github.xiaolyuh.cache.config.EnableLayeringCache;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableLayeringCache // 启用多级缓存框架
@MapperScan("com.qf.redisandes.mapper")
@EnableSwagger2
public class RedisandesApplication {

    public static void main(String[] args) {
        SpringApplication.run(RedisandesApplication.class, args);
    }

}
