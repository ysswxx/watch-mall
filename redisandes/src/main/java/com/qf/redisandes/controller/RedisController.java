package com.qf.redisandes.controller;

import com.qf.redisandes.pojo.ResultData;
import com.qf.redisandes.service.RedisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 作者: 官官
 * 时间：2021/3/31 18:20
 */
@RestController
public class RedisController {

    @Autowired
    RedisService rs;

    @GetMapping("/selectAll")

    public ResultData selectAll(@RequestParam(value = "page",defaultValue = "1") Integer page){


        return rs.selectAll(page);
    }


    @GetMapping("/insert")

    public ResultData insert(){


        return rs.insert();
    }

    @GetMapping("/delete")

    public ResultData delete(){


        return rs.delete();
    }


}
