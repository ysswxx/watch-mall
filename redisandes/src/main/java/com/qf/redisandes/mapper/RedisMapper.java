package com.qf.redisandes.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.redisandes.pojo.Watch;

/**
 * 作者: 官官
 * 时间：2021/3/31 18:24
 */
public interface RedisMapper extends BaseMapper<Watch> {
}
