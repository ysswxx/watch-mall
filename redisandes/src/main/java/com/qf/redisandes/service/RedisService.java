package com.qf.redisandes.service;

import com.qf.redisandes.pojo.ResultData;

/**
 * 作者: 官官
 * 时间：2021/3/31 18:23
 */
public interface RedisService {
    ResultData selectAll(Integer page);

    ResultData insert();

    ResultData delete();
}
