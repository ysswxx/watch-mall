package com.qf.redisandes.service.impl;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaolyuh.annotation.*;
import com.qf.redisandes.mapper.RedisMapper;
import com.qf.redisandes.pojo.ResultData;
import com.qf.redisandes.pojo.Watch;
import com.qf.redisandes.service.RedisService;

import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 作者: 官官
 * 时间：2021/3/31 18:24
 */
@Service
public class RedisServiceImpl implements RedisService {

    @Resource
    RedisMapper rm;
    @Override
    @Cacheable(value = "employee:info", key = "T(com.qf.redisandes.pojo.MD5Utils).md5('redisannes.selectall'+#page)"
            , depict = "用户信息缓存", enableFirstCache = true,
            firstCache = @FirstCache(expireTime = 400, timeUnit = TimeUnit.SECONDS),
            secondaryCache = @SecondaryCache(expireTime = 1000, preloadTime = 200,
                    forceRefresh = true, timeUnit = TimeUnit.SECONDS, isAllowNullValue = true, magnification = 100))
    public ResultData selectAll(Integer page) {
        System.out.println("查询数据库页码"+page);
        PageHelper.startPage(page,1);
        List<Watch> watches = rm.selectList(null);
        PageInfo<Watch> info = new PageInfo<>(watches);

        return ResultData.createSuccessJsonResult(info);
    }

    @Override

    @CachePut(value = "employee:info", key = "T(com.qf.redisandes.pojo.MD5Utils).md5('redisannes.selectall' + #watchId)"
            , depict = "用户信息缓存", enableFirstCache = true,
            firstCache = @FirstCache(expireTime = 400, timeUnit = TimeUnit.SECONDS),
            secondaryCache = @SecondaryCache(expireTime = 1000, preloadTime = 200,
                    forceRefresh = true, timeUnit = TimeUnit.SECONDS, isAllowNullValue = true, magnification = 100))
    public ResultData insert() {
        Watch watch = new Watch();

        watch.setCustomerId(2);
        watch.setUserId(2);
        watch.setWatchName("redis手表");
        watch.setCreateTime(new Date());
        int insert = rm.insert(watch);
        if (insert>0){
            return ResultData.createFailJsonResult("200","插入成功");
        }
        return ResultData.createFailJsonResult("500","插入失败");
    }

    @Override
    @CacheEvict(value = "employee:info", key = "employee:info")
    public ResultData delete() {

        return ResultData.createFailJsonResult("200","删除缓存成功");
    }
}
