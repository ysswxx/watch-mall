package com.qf.watchshop.userserver.controller;

import com.qf.watchshop.pojo.Users;
import com.qf.watchshop.userserver.service.UserService;
import com.qf.watchshop.userserver.utils.DxUtils;
import com.qf.watchshop.userserver.utils.ResultData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * 作者: 官官
 * 时间：2021/3/26 9:43
 */
@RestController

public class UserController {
    @Autowired
    RedisTemplate redisTemplate;
    @Autowired
    UserService us;
    @GetMapping("/dxcheck")
    public ResultData Dxcheck(String tel){

        DxUtils.SendeMessages(tel,redisTemplate);
        return new ResultData(200,"发送短信成功");


    }


    @PostMapping("/register")
    public ResultData register(@RequestBody Users users){

        return us.register(users);


    }


    @PostMapping("/login")
    public ResultData login(@RequestBody Users users){

        return us.login(users);


    }

    @GetMapping("/list")

    public String list(){

        return "list";


    }


}
