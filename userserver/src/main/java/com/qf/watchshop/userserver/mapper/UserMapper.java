package com.qf.watchshop.userserver.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fasterxml.jackson.databind.ser.Serializers;
import com.qf.watchshop.pojo.Users;
import org.springframework.stereotype.Repository;

/**
 * 作者: 官官
 * 时间：2021/3/26 9:04
 */
@Repository
public interface UserMapper extends BaseMapper<Users> {
}
