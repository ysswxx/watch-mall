package com.qf.watchshop.userserver.service;

import com.qf.watchshop.pojo.Users;
import com.qf.watchshop.userserver.utils.ResultData;

/**
 * 作者: 官官
 * 时间：2021/3/26 9:03
 */
public interface UserService {
    ResultData register(Users users);

    ResultData login(Users users);
}
