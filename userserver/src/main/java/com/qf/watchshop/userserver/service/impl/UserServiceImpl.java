package com.qf.watchshop.userserver.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.watchshop.pojo.Users;
import com.qf.watchshop.userserver.mapper.UserMapper;
import com.qf.watchshop.userserver.service.UserService;
import com.qf.watchshop.userserver.utils.DxUtils;
import com.qf.watchshop.userserver.utils.ResultData;
import com.qf.watchshop.userserver.utils.SaltUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * 作者: 官官
 * 时间：2021/3/26 9:03
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private  RedisTemplate redisTemplate;

    @Autowired
    UserMapper um;

    @Override
    public ResultData register(Users users) {


        //从redis获取验证码进行验证
        String code = DxUtils.GetMessages(users.getTel(), redisTemplate);
        if(code==null){

            return new ResultData(500,"验证码过期请重新注册");
        }else if(!code.equals(users.getCode())){
            return new ResultData(500,"验证码输入错误");
        }
        QueryWrapper<Users> qw = new QueryWrapper<>();
        qw.eq("username",users.getUsername());
        Users user1 = um.selectOne(qw);
        if (user1!=null){
            return new  ResultData(500,"用户名已存在,请重新注册");
        }


        users.setSalt(SaltUtils.getSalt());

        int i = um.insert(users);
        if (i>0){
            return new ResultData(200,"注册成功");
        }


        return   new ResultData(500,"注册失败");
    }

    @Override
    public ResultData login(Users users) {
        QueryWrapper<Users> qw = new QueryWrapper<>();
        qw.eq("username",users.getUsername());
        qw.eq("password",users.getPassword());
        Users users1 = um.selectOne(qw);







        return new ResultData(200,"登录成功");
    }


}
