package com.qf.watchshop.userserver.utils;


import com.taobao.api.ApiException;
import com.taobao.api.DefaultTaobaoClient;
import com.taobao.api.TaobaoClient;
import com.taobao.api.request.AlibabaAliqinFcSmsNumSendRequest;
import com.taobao.api.response.AlibabaAliqinFcSmsNumSendResponse;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

/**
 * 作者: 官官
 * 时间：2021/3/25 8:59
 */
@Component
public class DxUtils {


    public static ResultData SendeMessages(String phone,RedisTemplate redisTemplate) {
        String url = "http://gw.api.taobao.com/router/rest";
        String appkey = "23473071";
        String secret = "951efdcc9540d2c0c1646ed6d74892e6";
        TaobaoClient client = new DefaultTaobaoClient(url, appkey, secret);
        AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
        req.setExtend("123456");
        req.setSmsType("normal");
        req.setSmsFreeSignName("问道学院");
        String code = CodeUtil.getCode();
        req.setSmsParamString("{\"code\":\"" + code + "\"}");
        req.setRecNum(phone);
        req.setSmsTemplateCode("SMS_209190607");
        AlibabaAliqinFcSmsNumSendResponse rsp = null;
        try {
            rsp = client.execute(req);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        Boolean success = rsp.getResult().getSuccess();
        if (success) {
            //将验证码存放到redis中
            redisTemplate.boundValueOps("sms:" + phone).set(code);
            //设置过期时间(60s)
            redisTemplate.expire("sms:" + phone, 60, TimeUnit.SECONDS);


            return new ResultData(200, "发送短信成功");

        }
        return new ResultData(500, "发送短信失败");

    }


    public static String GetMessages(String phone,RedisTemplate redisTemplate){

      return (String) redisTemplate.boundValueOps("sms:" + phone).get();

    }


}
