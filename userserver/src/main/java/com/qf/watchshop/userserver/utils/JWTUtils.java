package com.qf.watchshop.userserver.utils;




import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Date;

public class JWTUtils {
    private static Long tokenExpireTime = 1000 * 60 * 30L;//  单位毫秒
//    public static final String PRIVATEKEY = "privateKey";
//    public static final String ACCESSTOKEN = "AccessToken";// 公私钥
    private static final String secretKey = "ueor82739sjsd234759jdfijosd289347sdjklfvjsxdr389wrksjdhfjksdyr9234yu89htsdkhfjksdhf83wy4hrsdjkhfsdjkh8i34wyuirfhsdjkfsxmnfbcvm,xcnskdhfriw3eyrikni12y391y238923y4y89dfhisfhsdjknfk23w4y598hfdjkfkjxcfbnisyer93we5rhkdjsfnjks"; // secret

    // 生成token
    public static String createToken(String jsonData){
        // 得到当前时间
        Date now = new Date();
        // 通过hs256算法，以及secretKey得到Algorithm对象
        Algorithm algo = Algorithm.HMAC256(secretKey);
        String token = JWT.create()
                .withIssuer("qianfeng")
                .withIssuedAt(now)
                .withExpiresAt(new Date(now.getTime() + tokenExpireTime))
                .withClaim("data", jsonData)//保存身份标识
                .sign(algo);
        return token;
    }

    // 验证token
    public static String verifyToken(String token){
        String data = null;
        try {
            Algorithm algorithm = Algorithm.HMAC256(secretKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer("qianfeng")
                    .build();
            DecodedJWT jwt = verifier.verify(token);
            data = jwt.getClaim("data").asString();
        } catch (JWTVerificationException e){
            e.printStackTrace();
        }
        return data;
    }

    public static void main(String[] args) {
//        System.out.println(createToken("张三"));
        String token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJkYXRhIjoie1wiaWRcIjowLFwidXNlcm5hbWVcIjpcIuW8oOS4iVwiLFwicGFzc3dvcmRcIjpudWxsfSIsImlzcyI6InFpYW5mZW5nIiwiZXhwIjoxNjE1MDAwMjg2LCJpYXQiOjE2MTQ5OTg0ODZ9.ifc8JSc-p2d7IHbl9LYA3XpcutmH8Sc44iZdwQ8Dyp0";
        System.out.println(verifyToken(token));
    }
}
