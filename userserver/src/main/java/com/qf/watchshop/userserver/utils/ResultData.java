package com.qf.watchshop.userserver.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 作者：官官
 * 时间：@date
 */
@Data

@NoArgsConstructor
public class ResultData<T> {
    private Integer code;
    private String msg;
    private T data;
    private Integer count;

    public ResultData(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public ResultData(Integer code, String msg,T data,Integer count) {
        this.code = code;
        this.msg = msg;
        this.data=data;
        this.count=count;
    }

    public static ResultData creatresult(Object o){

        return new ResultData(200,"查询成功",o,1);
    }

}
