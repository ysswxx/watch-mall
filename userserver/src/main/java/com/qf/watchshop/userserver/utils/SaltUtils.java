package com.qf.watchshop.userserver.utils;



import java.util.Random;

/**
 * 作者: 官官
 * 时间：2021/3/18 14:21
 */
public class SaltUtils {

    public static String getSalt(){

        String[] s={"!","@","#","$","%","^","&","*","(",")","a","b","w","q","e","t","y","u","i","m","d","o"
                ,"f","g","x","h","c","n","p","v","s","j","k","l","r","z","1","2",
                "3","4","5","6","7","8","9","0"};
        StringBuffer strBuilder = new StringBuffer();
        Random random = new Random();
        for (int i = 0; i < random.nextInt(8)/2 + 5; i++) {

            int i1 = random.nextInt(s.length-1);
            strBuilder.append(s[i1]);
        }

        return strBuilder.toString();
    }
}
